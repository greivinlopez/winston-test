'use strict';

const axios = require('axios');

// ---------------------- Helper Functions --------------------------
const capitalize = function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

/**
 * Find a person on the given employees object by matching firstname and lastname.
 * @param {string} firstName First name of the employee to search for.
 * @param {string} lastName Last name of the employee to search for.
 * @return {object} The object with the founded person's info. It returns undefined if not found.
 */
const findPerson = function(employees, firstName, lastName) {
    let findings = employees.filter(function (employee) {
        if (employee.firstName === capitalize(firstName) && employee.lastName === capitalize(lastName)) {
        	return true;
        } else {
        	let lastNameFirstWord = employee.lastName.substr(0, employee.lastName.indexOf(" "));
        	return employee.firstName === capitalize(firstName) && lastNameFirstWord === capitalize(lastName);
        }
    });
    if (findings.length > 0) {
        return findings[0];
    }
}

const filterVacations = function(timeOffItems) {
    let findings = timeOffItems.filter(function(timeOffItem) {
        return timeOffItem.name === 'Vacation'
    });
    if (findings.length > 0) {
        return findings[0];
    }
}
// ------------------------------------------------------------------

// ************************* BambooHR API ***************************

const apiKey = 'xxxxx secret key';
const subDomain = 'subdomain';

/**
 * Base configuration for BambooHR API requests
 */
const bambooAPI = axios.create({
	baseURL: 'https://api.bamboohr.com/api/gateway.php/'+subDomain,
	auth: {
    	username: apiKey,
    	password: 'x'
  	},
  	timeout: 6000,
	headers: {
        'accept': 'application/json',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.8'
    }
});

/**
 * Gets the complete list of employees from the BambooHR system.
 * @param {function} doSomething Callback function expected to do something with the list of employees
 */
const getEmployees = function (doSomething) {
	bambooAPI.get('/v1/employees/directory')
	.then(function (response) {
		doSomething(response.data.employees);
	})
	.catch(function (error) {
		console.log(error);
	});
}

/**
 * Gets the timeoff balance of the given employee
 * @param {string} firstName First name of the employee to retrieve his/her timeoff balance.
 * @param {string} lastName Last name of the employee to retrieve his/her timeoff balance.
 */
const getDays = function (firstName, lastName) {
	getEmployees( function(employees) {
		var person = findPerson(employees, firstName, lastName);

		if (person === undefined) {
			console.log(firstName + ' ' + lastName + ' could not be found.');
			return
		}

		var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        bambooAPI.get('/v1/employees/'+person.id+'/time_off/calculator/?end='+yyyy+ '-'+mm+'-'+dd)
		.then(function (response) {
			console.log(response.data);
		})
		.catch(function (error) {
			console.log(error);
		});
	});
}

/**
 * Gets the upcoming holidays and information regarding employees that are currently ooo
 */
const getComingHolidays = function() {
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

	bambooAPI.get('/v1/time_off/whos_out/?start='+yyyy+ '-'+mm+'-'+dd)
	.then(function (response) {
		console.log(response.data);
	})
	.catch(function (error) {
		console.log(error);
	});
}

getDays("Greivin", "Moraga");